# MODULO PARA BACKEND - TERRAFORM #

Este Módulo contempla a estrutura base de um backend remoto utilizando o terraform

### O que compõe o backend ? ###

* 1 Bucket s3 para armazenamento do state file 
* 1 tabela no DynamoDB para realizar o lock durante a utilização simultânea do terraform


### Como Utilizar ? ###

* Realizar o clone do repositório
* editar o arquivo backend.tfvars localizado no path samples/


region  = Região onde os recursos serão criados           
credential_profile = credenciais de acesso programatico com as permissões necessárias 
account_id  = ID da Conta       
environment = Referência do ambiente (dev, hml ou prd)       
backend_dynamodb_table_name = Não preencher
backend_bucket_name = Não preencher        

