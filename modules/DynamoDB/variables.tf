variable "backend_dynamodb_table_name" {
  type        = string
  description = "Nome da tabela Dynamo para realizar o travamento do estado tfsate, bloqueando execuções simultâneas."
}

variable "environment" {
  type        = string
  description = ""
}
