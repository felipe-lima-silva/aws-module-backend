

resource "aws_dynamodb_table" "backend_lock_tfstate" {
  
  name  = var.backend_dynamodb_table_name

  billing_mode   = "PAY_PER_REQUEST"
  read_capacity  = null
  write_capacity = null

  # https://www.terraform.io/docs/backends/types/s3.html#dynamodb_table
  hash_key = "LockID"

  server_side_encryption {
    enabled = true
  }

  point_in_time_recovery {
    enabled = false
  }

  attribute {
    name = "LockID"
    type = "S"
  }

  lifecycle {
    prevent_destroy = false
  }
}


