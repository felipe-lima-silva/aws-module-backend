resource "aws_s3_bucket_public_access_block" "block" {
    bucket = aws_s3_bucket.b.id

    block_public_acls   = true
    block_public_policy = true
    restrict_public_buckets = true
    ignore_public_acls = true
  }


resource "aws_s3_bucket" "b" {
  bucket = var.backend_bucket_name

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  versioning {
    enabled = true
  }

  tags = {
    Name        = "Backend"
    Environment = var.environment
  }
}

