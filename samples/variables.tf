variable "region" {
  type        = string
  description = ""
}

variable "environment" {
  type        = string
  description = ""
}

variable "backend_bucket_name" {
  type        = string
  description = ""
}

variable "account_id" {
  type        = string
  description = ""
}

variable "credential_profile" {
  type        = string
  description = ""
}

variable "backend_dynamodb_table_name" {
  type        = string
  description = "Nome da tabela Dynamo para realizar o travamento do estado tfsate, bloqueando execuções simultâneas."
}




