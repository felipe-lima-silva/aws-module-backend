
module "backend-DB" {
  source = "../modules/DynamoDB/"

  backend_dynamodb_table_name = "lock-table-${var.account_id}-${var.environment}"
  environment                 = var.environment
}

module "backend-s3" {
  source = "../modules/s3/"

  backend_bucket_name = "state-file-${var.account_id}-${var.environment}"
  environment         = var.environment
}